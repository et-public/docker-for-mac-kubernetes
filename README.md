# Getting Started with Docker-for-Mac with Kubernetes

## Table of Contents ##
- [Install Docker for Mac](#install-docker-for-mac)
- [Enable the kubernetes cluster](#enable-the-kubernetes-cluster)
- [Install kubectl and kubectx](#install-kubectl-and-kubectx)
- [Switch kubernetes context](#switch-kubernetes-context)
- [Getting to know the cluster](#getting-to-know-the-cluster)
- [Install kubernetes-dashboard application](#install-kubernetes-dashboard-application)
- [Install a guestbook 3 tier application](#install-a-guestbook-3-tier-application)
- [Access services by port-forwarding](#access-services-by-port-forwarding)
- [Deploy to Kubernetes from docker-compose file](#deploy-to-kubernetes-from-docker-compose-file)
- [Teardown](#teardown)

## Install Docker for Mac

Docker CE Edge installer comes with experimental features in Docker Engine enabled by default and these features are configurable on Docker Daemon preferences.

Go [docker-for-mac Edge](https://download.docker.com/mac/edge/Docker.dmg) to install it. This tutorial is tested with Docker for Mac using Edge channel. 

## Enable the Kubernetes cluster

Click the `Docker` icon in the status bar, go to `Preferences`, and on the `Kubernetes` tab,  check *Enable Kubernetes*. This will start a single node Kubernetes cluster. This might take a while - have a cup of tea and wait for the Kubernetes cluster to be ready.

Docker for Mac Edge kubernetes integration installed `kubectl v.1.9` command in /usr/local/bin, which overrides what's installed in by brew. We need the latest `kubectl` 
for some of the Kubernetes feature to work. Let's fix it and make sure `kubectl` version is 1.10.x:

```console
$ rm /usr/local/bin/kubectl
$ brew reinstall kubectl
$ kubectl version -c
Flag shorthand -c has been deprecated, please use --client instead.
Client Version: version.Info{Major:"1", Minor:"10", GitVersion:"v1.10.1", GitCommit:"d4ab47518836c750f9949b9e0d387f20fb92260b", GitTreeState:"clean", BuildDate:"2018-04-13T22:29:03Z", GoVersion:"go1.9.5", Compiler:"gc", Platform:"darwin/amd64"}
```

## Install kubectl and kubectx

[Kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) is a Kubernetes cli to interact with kubernetes cluster. [kubectx](https://github.com/ahmetb/kubectx) helps you switch between clusters back and forth.

```console
$ brew install kubectl kubectx
```

Both commands will be installed in /usr/local/bin. 

You may have other versions of kubectl in your command path (/opt/bin, $HOME/bin..), make sure to use the one in /usr/local/bin. `/usr/local/bin/kubectl version` should show v1.10.0. Although other kubectl versions may also work, the v1.10.0 has the service port forwarding feature we will need later.

## Switch kubernetes context

If you have been working with Kubernetes, you may have multiple cluster configurations in your $HOME/.kube directory. These kubeconfig files contain clusters's API server URL, user credentials etc. You can use *kubectx* to list, show, and switch context. Run `kubectx -h` for details.

* List kubernetes contexts

```console
$ kubectx
docker-for-desktop
* eks-preview
kube-lab
kubev1
```

The one with the "*" (or highlighted)  is the current context. To swtich to docker-for-desktop:

* Switch context

```console
$ kubectx docker-for-desktop
Switched to context "docker-for-desktop".
```

* Get kubernetes docker-for-descktop context details:

```console
$ kubectx docker-for-desktop
$ kubectl config view --minify
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: https://localhost:6443
  name: docker-for-desktop-cluster
contexts:
- context:
    cluster: docker-for-desktop-cluster
    user: docker-for-desktop
  name: docker-for-desktop
current-context: docker-for-desktop
kind: Config
```

## Getting to know the cluster

* To verify local kubernetes cluster is running

```console
$ kubectl cluster-info
Kubernetes master is running at https://localhost:6443
KubeDNS is running at https://localhost:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

* List what's running by default installation

```console
$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                         READY     STATUS    RESTARTS   AGE
docker        compose-5d4f4d67b6-wkfkm                     1/1       Running   0          28m
docker        compose-api-7bb7b5968f-j88m4                 1/1       Running   0          28m
kube-system   etcd-docker-for-desktop                      1/1       Running   0          29m
kube-system   kube-apiserver-docker-for-desktop            1/1       Running   0          29m
kube-system   kube-controller-manager-docker-for-desktop   1/1       Running   0          29m
kube-system   kube-dns-6f4fd4bdf-mh6ng                     3/3       Running   0          29m
kube-system   kube-proxy-2bz9q                             1/1       Running   0          29m
kube-system   kube-scheduler-docker-for-desktop            1/1       Running   0          29m
```

## Install kubernetes-dashboard application

Let's install "kubernetes-dashboard". This allows us to use UI to manage the cluster, view workload status, debug deployment problems etc.

```console
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
secret "kubernetes-dashboard-certs" created
serviceaccount "kubernetes-dashboard" created
role "kubernetes-dashboard-minimal" created
rolebinding "kubernetes-dashboard-minimal" created
deployment "kubernetes-dashboard" created
service "kubernetes-dashboard" created
```

Now run `kubectl proxy`  so we can connect to UI from our laptop. In case you have left-over proxy running, kill it first, then start a new one, as shown below:

```console
$ pkill kubectl
$ kubectl proxy
Starting to serve on 127.0.0.1:8001
```

Then go to the [kubernetes-dashboard](http://127.0.0.1:8001/ui/). Click "SKIP" in the login page, you should see the dashboard:

![Kubernetes-dashboard](./images/dashboard.png)

Now explore key concepts of Kubernetes: Namespaces, Pods, Secrets etc. Note that you are in "default" namesapces when you sign-in. Switch to "kube-system", you will see a lot more workloads running in that namespace. Click on "Nodes", you can see a graphical overview of "docker-for-desktop" one-node cluster - its resource usage etc.

![docker-for-desktop node](./images/node.png)

## Install a guestbook 3 tier application

We are going to install a native Kubernetes application [guestbook]([https://kubernetes.io/docs/tutorials/stateless-application/guestbook/). The link gives step by step deployment to install redis-master, redis-slave, guestbook application and service load-balancer. You can go ahead to follow that documentation or just do the installation with all-in-one yaml file:

```console
$ kubectx docker-for-desktop
$ kubectl create -f https://raw.githubusercontent.com/kubernetes/examples/master/guestbook/all-in-one/guestbook-all-in-one.yaml
service "redis-master" created
deployment "redis-master" created
service "redis-slave" created
deployment "redis-slave" created
service "frontend" created
deployment "frontend" created
```

The guestbook application is created in "Default" namespace, you can see it on UI or run the following command to slice-and-dice the application view, with their "label" defined in their kubernetes manifest. 

* Frontend: loadbalancer svc/frontend, and 3 replicas to serve the application

```console
$ kubectl get deploy,svc,pods  -n default -l tier=frontend
NAME              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deploy/frontend   3         3         3            3           5m

NAME           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
svc/frontend   ClusterIP   10.105.182.163   <none>        80/TCP    5m

NAME                          READY     STATUS    RESTARTS   AGE
po/frontend-67f65745c-dmkzq   1/1       Running   0          5m
po/frontend-67f65745c-n24fq   1/1       Running   0          5m
po/frontend-67f65745c-nzmr4   1/1       Running   0          5m
```

* Backend: One replica of redis-master, 3 redis-slave

```console
$ kubectl get deploy,svc,pods  -n default -l tier=backend
NAME                  DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deploy/redis-master   1         1         1            1           8m
deploy/redis-slave    2         2         2            2           8m

NAME               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
svc/redis-master   ClusterIP   10.103.215.140   <none>        6379/TCP   8m
svc/redis-slave    ClusterIP   10.105.74.181    <none>        6379/TCP   8m

NAME                               READY     STATUS    RESTARTS   AGE
po/redis-master-585798d8ff-tqcpn   1/1       Running   0          8m
po/redis-slave-865486c9df-7kd75    1/1       Running   0          8m
po/redis-slave-865486c9df-b45sq    1/1       Running   0          8m
```

## Access services by port-forwarding

Here we will demonstrate `kubectl port-forward` feature. We will connect to the Redis master server we just deployed for the guestbook app. Even the port 6379 is not open to the Internet, we can connect to the server by port-forwarding.

- Access to redis-master

```console
$ kubectl port-forward svc/redis-master 6379:6379
Forwarding from 127.0.0.1:6379 -> 6379
Handling connection for 6379
```

On another terminal on your local machine, the `redis-cli` command talks to redis master through Kubernetes proxy:

```console
$ brew upgrade redis || brew install redis
$ redis-cli ping
PONG
```

```console
$ redis-cli info |grep redis_
redis_version:2.8.19
redis_git_sha1:00000000
redis_git_dirty:0
redis_build_id:a74551038794e13f
redis_mode:standalone
```

```console
$  redis-cli info |grep db
rdb_changes_since_last_save:0
rdb_bgsave_in_progress:0
rdb_last_save_time:1523552802
rdb_last_bgsave_status:ok
rdb_last_bgsave_time_sec:0
rdb_current_bgsave_time_sec:-1
```

Or just get inside of the redis master:

```console
$ redis-cli
127.0.0.1:6379>
quit
```

You can Control-C to terminate the port-forward proxy in the other terminal.

- Access guestbook

When we start the guestbook application, the frontend container register its service name and port with Kubernetes
dns service, as shown blow:

```console
$ kubectl get svc -l app=guestbook
NAME       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
frontend   ClusterIP   10.105.182.163   <none>        80/TCP    54m

$ kubectl port-forward svc/frontend 8000:80
Forwarding from 127.0.0.1:8000 -> 80
Forwarding from [::1]:8000 -> 80
Handling connection for 8000
Handling connection for 8000
```

In this example the `frontend` service uses "ClusterIP" service type running on IP 10.105.182.163:80, which means
it is only accessible by pods in the cluster.  The service name is `frontend.default.svc.cluster.local`.
When frontend containers move, the service IP will not change. This function is provided by `kube-dns` service.

There are 3 pods servicing the frontend. They are at 10.1.0.6:80,10.1.0.8:80,10.1.0.9:80, as shown below:

```console
$ kc describe  svc frontend
Name:              frontend
Namespace:         default
Labels:            app=guestbook
                   tier=frontend
Annotations:       <none>
Selector:          app=guestbook,tier=frontend
Type:              ClusterIP
IP:                10.96.34.106
Port:              <unset>  80/TCP
TargetPort:        80/TCP
Endpoints:         10.1.0.6:80,10.1.0.8:80,10.1.0.9:80
Session Affinity:  None
Events:            <none>
```

To access from outside of the kubernetes cluster network, without expose it to Internet, we can use port-forwarding:

```console
$ kubectl get svc  -l app=guestbook
NAME       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
frontend   ClusterIP   10.105.182.163   <none>        80/TCP    54m

$ kubectl port-forward svc/frontend 8000:80
Forwarding from 127.0.0.1:8000 -> 80
Forwarding from [::1]:8000 -> 80
Handling connection for 8000
Handling connection for 8000
```

Now you can connect to the guestbook application on http://127.0.0.1:8000 and sign-up as a guest, as shown blow:

![Guestbook](./images/guestbook.png)

You can terminate the proxy by Control-C to in the proxy window.

## Deploy to Kubernetes from docker-compose file

We are going to demo how to deploy docker-compose file to Kubernetes cluster. 

- Deploy a words web application

```console
$ git clone https://code.stanford.edu/sfeng/docker-for-mac-kubernetes
$ cd docker-for-mac-kubernetes/examples
$ DOCKER_ORCHESTRATOR=kubernetes docker stack deploy docker-web --compose-file docker-compose.yml
Ignoring unsupported options: build

Stack docker-web was created
Waiting for the stack to be stable and running...
 - Service words has one container running
 - Service db has one container running
 - Service web has one container running
Stack docker-web is stable and running
```

- Verify pods and service are running

```console
$ docker stack services docker-web
ID                  NAME                MODE                REPLICAS            IMAGE                   PORTS
f37955ad-3e8        docker-web_db       replicated          1/1                 dockerdemos/lab-db
f37c0e41-3e8        docker-web_web      replicated          1/1                 dockerdemos/lab-web     *:80->80/tcp
f38e0067-3e8        docker-web_words    replicated          5/5                 dockerdemos/lab-words
```

Or if you want to use kubectl:

```console
$ kubectl get pods,svc
NAME                        READY     STATUS    RESTARTS   AGE
po/db-cc6d959d-t2j8l        1/1       Running   0          1m
po/web-bf8c55f48-56d2v      1/1       Running   0          1m
po/words-657d8f455b-bv6vk   1/1       Running   0          1m
po/words-657d8f455b-f9bjf   1/1       Running   0          1m
po/words-657d8f455b-lkdll   1/1       Running   0          1m
po/words-657d8f455b-ltq26   1/1       Running   0          1m
po/words-657d8f455b-t85ls   1/1       Running   0          1m

NAME                TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
svc/db              ClusterIP      None           <none>        55555/TCP      1m
svc/kubernetes      ClusterIP      10.96.0.1      <none>        443/TCP        3h
svc/web             ClusterIP      None           <none>        55555/TCP      1m
svc/web-published   LoadBalancer   10.110.27.41   localhost     80:31119/TCP   1m
svc/words           ClusterIP      None           <none>        55555/TCP      1m
```

`svc/web-published` is the frontend that exposed to external host (LoadBalancer type). Just point your browser to
http://localhost:80. You get `hello-world!`

- To update the content, edit the web/static/index.html, then

```console
$ DOCKER_ORCHESTRATOR=kubernetes docker stack up docker-web --compose-file docker-compose.yml
```

- List stacks

```console
 $ docker stack ls
 NAME                SERVICES
docker-web          3
```

## Teardown

- Delete the docker-web applicaiton if you deployed it

```console
$ DOCKER_ORCHESTRATOR=kubernetes docker stack rm docker-web
Removing stack: docker-web
```

- Delete guestbook application

```console
$ kubectl delete -f  https://raw.githubusercontent.com/kubernetes/examples/master/guestbook/all-in-one/guestbook-all-in-one.yaml

service "redis-master" deleted
deployment "redis-master" deleted
service "redis-slave" deleted
deployment "redis-slave" deleted
service "frontend" deleted
deployment "frontend" deleted
```

* Delete dashboard

```console
$ kubectl delete -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml

secret "kubernetes-dashboard-certs" deleted
serviceaccount "kubernetes-dashboard" deleted
role "kubernetes-dashboard-minimal" deleted
rolebinding "kubernetes-dashboard-minimal" deleted
deployment "kubernetes-dashboard" deleted
service "kubernetes-dashboard" deleted
```

NOTE: Although you have deleted all the kuberntes workload you installed, your cluster is still running:

```console
$ kc get deployment --all-namespaces
NAMESPACE     NAME          DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
docker        compose       1         1         1            1           3h
docker        compose-api   1         1         1            1           3h
kube-system   kube-dns      1         1         1            1           3h
```

You can go to `Docker->Preference->Kubernetes` on status bar and disable kurbernetes in Docker. These system deployment will go away.

## What's next

You now have a Kubernetes cluster on your laptop. You can play, learn, develope your kubernetes applications before you put the load on a production kubernetes system.

You can use the cluster you just enable to complete [Kubernetes tutorial](https://kubernetes.io/docs/tutorials/).

